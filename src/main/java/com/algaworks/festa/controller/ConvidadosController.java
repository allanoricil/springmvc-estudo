package com.algaworks.festa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.algaworks.festa.model.Convidado;
import com.algaworks.festa.repository.Convidados;

@Controller
public class ConvidadosController {
	
	@Autowired
	private Convidados convidados;
	
	@GetMapping("/convidados")
	public ModelAndView listar(){
		ModelAndView modelAndView = new ModelAndView("ListaConvidados");
		
		modelAndView.addObject("convidados", convidados.findAll());
		
		modelAndView.addObject(new Convidado());
		
		return modelAndView;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/convidados/{id}")
	public @ResponseBody Convidado listarUm(@PathVariable("id") Long id){
		Convidado convidado = convidados.findOne(id);
		return convidado;
	}
	
	@PostMapping("/convidados")
	public String salvar(Convidado convidado){
		this.convidados.save(convidado);
		return "redirect:/convidados";
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/convidados/{id}")
	public String delete(@PathVariable("id") Long id){
		Convidado convidado = convidados.findOne(id);
		this.convidados.delete(convidado);
		return "redirect:/convidados";
	}
	
}
